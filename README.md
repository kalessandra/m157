# Druckserver konfigurieren mit einem Raspi

# Table of contents
1. [Einleitung](#Einleitung)
2. [Installation](#applikationen1)
3. [Tests](#applikationen2)
4. [Hürden](#applikationen3)
5. [Abnahmeprotokoll](#applikationen4)


## Einleitung <a name="Einleitung"></a>
Wir wollten Docker auf einem Raspberry einen CUPS-Druckerserver einrichten. Dafür installieren wir als erstes Docker und richten dann in einem Docker-File CUPS ein. Da dies jedoch schlusseindlich aus unbekannten Gründen fehlschlug und wir unsere Zeit in die erfolglose Fehlerbehebung gesteckt hatten, handelten wir mit dem Lehrer aus, das ganze lediglich ohne Docker zu machen. <br> <br>

HW-Anforderungen: Raspy und Drucker

## Installation <a name="applikationen1"></a>

**Installation** <br>
Als erstes muss dem Gerät eine statische IP gegeben werden. Dazu öffnet man den DHCP Client Deamon. <br>
```
sudo nano /etc/dhcpcd.conf

```
Danach muss man die folgenden Zeilen anhängen:

```
interface eth0
static ip_address=ipadresse/24
static routers=routerip
static domain_name_servers=dnsip

```
Nachdem man diese Einstellung gemacht hat, muss man den Raspi neustarten.<br>
Danach installieren wir Cups.
```
sudo apt-get install cups cups-client

```
Nach der Installation von Cups haben wir folgenden Befehl verwendet um die Treiber zu installieren:
```
sudo apt-get install hplip printer-driver-hpijs printer-driver-gutenprint

```
* hplip (für HP-Drucker, egal ob Laser- oder Tintenstahldrucker)
* printer-driver-hpijs (wird für hplib benötigt)
* printer-driver-guntenprint (enthält viele Treiber für sonstige Farb-Tintendrucker)

Nun starten wir den Dienst.
```
sudo /etc/init.d/cups start

```
Um andere Benutzer für die Druckerkonfiguration zu berechtigen, muss dieser Benutzer in der Gruppe lpadmin sein. <br>

```
sudo usermod -aG lpadmin Benutzername

```
Es bietet sich zudem an, einen extra Drucker-Benutzer anzulegen:
```
sudo useradd -c Druckeruser -M -s /bin/false Drucker
sudo usermod -aG lpadmin Drucker
sudo passwd Drucker

```
**CUPS konfigurieren:** <br>
Die Konfigurationsdatei unter ***/etc/cups/cupsd.conf*** muss noch angepasst werden. Unten ist die angepasste Conf Datei.

**Cupsd.conf**
```

#
# Configuration file for the CUPS scheduler.  See "man cupsd.conf" for a
# complete description of this file.
#

# Log general information in error_log - change "warn" to "debug"
# for troubleshooting...
LogLevel warn
PageLogFormat

# Deactivate CUPS' internal logrotating, as we provide a better one, especially
# LogLevel debug2 gets usable now
MaxLogSize 0

# Only listen for connections from the local machine.
# Listen localhost:631
# Listen /var/run/cups/cups.sock

#Allow remote access
Port 631

# Show shared printers on the local network.
Browsing On
BrowseLocalProtocols dnssd

# Default authentication type, when authentication is required...
DefaultAuthType Basic

# Web interface setting...
WebInterface Yes

# Restrict access to the server...
<Location />
  Require user @SYSTEM
  Order allow,deny
  Allow @Local
</Location>

# Restrict access to the admin pages...
<Location /admin>
  Order allow,deny
  Allow @Local
  Require user @SYSTEM
</Location>

# Restrict access to configuration files...
<Location /admin/conf>
  AuthType Default
  Require user @SYSTEM
  Order allow,deny
  Allow @Local
</Location>

<Location /classes>
  Order allow,deny
  Allow @Local
  Require user @SYSTEM
</Location>

<Location /help>
  Order allow,deny
  Allow @Local
  Require user @SYSTEM
</Location>

<Location /jobs>
  Order allow,deny
  Allow @Local
  Require user @SYSTEM
</Location>

#Restrict access to printers

<Location /printers>
  Order allow,deny
  Allow @Local
  #Require user @SYSTEM
</Location>


# Restrict access to log files...
<Location /admin/log>
  AuthType Default
  Require user @SYSTEM
  Order allow,deny
</Location>

# Set the default printer/job policies...
<Policy default>
  # Job/subscription privacy...
  JobPrivateAccess default
  JobPrivateValues default
  SubscriptionPrivateAccess default
  SubscriptionPrivateValues default

  # Job-related operations must be done by the owner or an administrator...
  <Limit Create-Job Print-Job Print-URI Validate-Job>
    Order deny,allow
  </Limit>

  <Limit Send-Document Send-URI Hold-Job Release-Job Restart-Job Purge-Jobs Set-Job-Attributes Create-Job-Subscription Renew-Subscription Cancel-Subscription Get-Notifications Reprocess-Job Cancel-Current-Job Suspend-Current-Job Resume-Job Cancel-My-Jobs Close-Job CUPS-Move-Job CUPS-Get-Document>
    Require user @OWNER @SYSTEM
    Order deny,allow
  </Limit>

  # All administration operations require an administrator to authenticate...
  <Limit CUPS-Add-Modify-Printer CUPS-Delete-Printer CUPS-Add-Modify-Class CUPS-Delete-Class CUPS-Set-Default CUPS-Get-Devices>
    AuthType Default
    Require user @SYSTEM
    Order deny,allow
  </Limit>

  # All printer operations require a printer operator to authenticate...
  <Limit Pause-Printer Resume-Printer Enable-Printer Disable-Printer Pause-Printer-After-Current-Job Hold-New-Jobs Release-Held-New-Jobs Deactivate-Printer Activate-Printer Restart-Printer Shutdown-Printer Startup-Printer Promote-Job Schedule-Job-After Cancel-Jobs CUPS-Accept-Jobs CUPS-Reject-Jobs>
    AuthType Default
    Require user @SYSTEM
    Order deny,allow
  </Limit>

  # Only the owner or an administrator can cancel or authenticate a job...
  <Limit Cancel-Job CUPS-Authenticate-Job>
    Require user @OWNER @SYSTEM
    Order deny,allow
  </Limit>

  <Limit All>
    Order deny,allow
  </Limit>
</Policy>

# Set the authenticated printer/job policies...
<Policy authenticated>
  # Job/subscription privacy...
  JobPrivateAccess default
  JobPrivateValues default
  SubscriptionPrivateAccess default
  SubscriptionPrivateValues default

  # Job-related operations must be done by the owner or an administrator...
  <Limit Create-Job Print-Job Print-URI Validate-Job>
    AuthType Default
    Order deny,allow
  </Limit>

  <Limit Send-Document Send-URI Hold-Job Release-Job Restart-Job Purge-Jobs Set-Job-Attributes Create-Job-Subscription Renew-Subscription Cancel-Subscription Get-Notifications Reprocess-Job Cancel-Current-Job Suspend-Current-Job Resume-Job Cancel-My-Jobs Close-Job CUPS-Move-Job CUPS-Get-Document>
    AuthType Default
    Require user @OWNER @SYSTEM
    Order deny,allow
  </Limit>

  # All administration operations require an administrator to authenticate...
  <Limit CUPS-Add-Modify-Printer CUPS-Delete-Printer CUPS-Add-Modify-Class CUPS-Delete-Class CUPS-Set-Default>
    AuthType Default
    Require user @SYSTEM
    Order deny,allow
  </Limit>

  # All printer operations require a printer operator to authenticate...
  <Limit Pause-Printer Resume-Printer Enable-Printer Disable-Printer Pause-Printer-After-Current-Job Hold-New-Jobs Release-Held-New-Jobs Deactivate-Printer Activate-Printer Restart-Printer Shutdown-Printer Startup-Printer Promote-Job Schedule-Job-After Cancel-Jobs CUPS-Accept-Jobs CUPS-Reject-Jobs>
    AuthType Default
    Require user @SYSTEM
    Order deny,allow
  </Limit>

  # Only the owner or an administrator can cancel or authenticate a job...
  <Limit Cancel-Job CUPS-Authenticate-Job>
    AuthType Default
    Require user @OWNER @SYSTEM
    Order deny,allow
  </Limit>

  <Limit All>
    Order deny,allow
  </Limit>
</Policy>

```
Danach müssen wir noch die Cups neu starten.
```
sudo /etc/init.d/cups restart
```

Um auf das Webinterface verbinden zu können, muss der Notebook im gleichen Netz sein. Man verbindet via https://fixe-ip-adresse-raspi:631. Man muss auch die Logindaten angeben! <br> <br>
Die Konfiguration der Datei ***/etc/cups/cupsd.conf*** ist im Webbrowser etwas einfacher zu bedienen. <br>
Die Anmeldung erfolgt mit dem User pi.
![picture](pictures/anmeldung.png) <br>
**Drucker einrichten** <br>
Um den Drucker einrichten zu können, muss der Drucker via USB am Rasperry angeschlossen sein. <br>
Nun kann man im Register Verwaltung den lokalen Drucker hinzufügen:<br>
Dazu wählen wir den HP-Printer Drucker via USB.
![picture](pictures/drucker.png) <br>
Danach auf Weiter.<br>
Danach sollte man einen sinnvollen Namen für den Ort geben, z.B. U65. Zusätzlich muss die Option "Freigabe" bzw. "Shared" angewählt werden, da sonst der Drucker später nicht im Netzwerk auffindbar ein wird. Danach auf Weiter.
![picture](pictures/drucker1.png) <br>
CUPS schlägt dann einen geeigneten Druckertreiber vor. Hier muss man etwas ausprobieren. Wenn später der Druck von einem Clienten nicht funktioniert, ist der Treiber wahrscheindlich falsch. Die PCL-Treiber funktionieren besser als die CUPS-Treiber.
![picture](pictures/drucker2.png) <br> <br>
Mit "Drucker Hinzufügen" ist die Konfiguration beim Raspi beendet. <br> <br>

Nun kann man im Register Drucker, Feld "Wartung" eine Testseite ausdrucken. Bitte den Eintrag in der Warteschlange beachten. <br>
Achtung:: Dies kann sehr lange dauern. <br> <br>
**Konfiguration Windows Client** <br>
Damit über Cup Printserver gedruckt werden kann, muss der Internetdruckclient und der LPR-Anschlussmonitor installiert sein. <br>
Vorgehen: <br>
Systemsteuerung -> Programme und Features -> Windows-Features aktivieren oder deaktivieren -> Druck- und Dokumentendienste -> Internetdruckclient, hier muss ein Häkchen gesetzt sein. <br>
![picture](pictures/windowsclient.png) <br>
Um weiteren Problemen vorzubeugen, muss noch eine Firewallregel erstellt werden, welche den Port UDP 631 öffnet. <br>
Anschliessend muss man in der Systemsteuerung den Drucker hinzufügen:
![picture](pictures/druckerhinzufuegenwindows.png) <br>


## Tests <a name="applikationen2"></a>
Als Test haben wir von unserem Windows Client den Projektantrag ausgedruckt. Das hat glücklicherweise funktioneirt. 

## Hürden <a name="applikationen3"></a>
Wir hatten zuerst mit Docker einige Probleme, die wir schlussendlich nicht beheben konnten. Wir konnten Docker aus bisher unbekannten Gründen nicht starten. <br> <br>

Des Weiteren hatten wir ständig das Problem, dass wir nicht den richtigen Access Point verbunden haben. Aus Schussligkeit vergassen wir immer wieder, dass wir mit dem LAB-U60 verbunden werden müssen.

## Abnahmeprotokoll <a name="applikationen4"></a>

cups wurde auf dem Raspi installiert -- JA <br>
Drucker wurde verbunden -- JA <br>
Es kann gedruckt werden -- JA <br>
