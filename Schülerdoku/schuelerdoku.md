# Druckserver konfigurieren mit einem Raspi Anleitung für Schüler

# Table of contents
1. [Einleitung](#Einleitung)
2. [Installation](#applikationen1)


## Einleitung <a name="Einleitung"></a>
Wir wollen zusammen mit dem Raspberry Pi einen Druckserver aufsetzen, damit wir alle Drucker zentral ansteuern können. Dafür verwenden wir den von Apple entwickelten Dienst CUPS der das Ziel hat, auf allen Systemen kompatibel zu sein.
Wir stellen uns als Ausgangslage vor, ein Geschäft hat nur USB-Drucker und seine Mitarbeiter rennen ständig zu den Druckern, wenn sie etwas ausdrucken wollen.<br>

## Installation <a name="applikationen1"></a>

**Installation** <br>
Als erstes muss dem Gerät eine statische IP gegeben werden.<br>

Danach muss man die folgenden Zeilen anhängen:

```
interface eth0
static ip_address=ipadresse+subnetzmaske
static routers=routerip
static domain_name_servers=dnsip

```
Nachdem man diese Einstellung gemacht hat, muss man den Raspi möglicherweise neustarten.<br>
Danach installieren wir Cups. <br>

Nach der Installation von Cups haben müssen folgende Treiber installiert werden:

* hplip (für HP-Drucker, egal ob Laser- oder Tintenstahldrucker)
* printer-driver-hpijs (wird für hplib benötigt)
* printer-driver-guntenprint (enthält viele Treiber für sonstige Farb-Tintendrucker)

Nun starten wir den Dienst. <br>

Um andere Benutzer für die Druckerkonfiguration zu berechtigen, muss dieser Benutzer in der Gruppe lpadmin sein. <br>


Es empfiehlt sich, einen extra Drucker-Benutzer anzulegen:

**CUPS konfigurieren:** <br>
Die Konfigurationsdatei unter ***/etc/cups/cupsd.conf*** muss noch den Bedürfnissen des Kunden angepasst werden. <br>

Danach müssen wir noch die Cups neu starten.
```
sudo /etc/init.d/cups restart
```

Um auf das Webinterface zugreifen zu können, muss der Notebook im gleichen Netz wie das Raspberry Pi sein. Man verbindet sich damit via HTTPS. <br> <br>
Auf dem Webinterface kann man das ganze auch auf einem GUI bearbeiten <br>

**Drucker einrichten** <br>
Um den Drucker einrichten zu können, muss der Drucker via USB am Rasperry angeschlossen sein. <br> Nun kann man im Register Verwaltung den lokalen Drucker hinzufügen.<br>

Danach auf Weiter.<br> Danach sollte man einen sinnvollen Namen für den Ort geben, z.B. U65. Zusätzlich muss der Drucker freigegeben werden. <br>

CUPS schlägt dann einen geeigneten Druckertreiber vor. Hier muss man etwas ausprobieren. Wenn später der Druck von einem Clienten nicht funktioniert, ist der Treiber wahrscheindlich falsch. Die PCL-Treiber funktionieren besser als die CUPS-Treiber.
Mit "Drucker Hinzufügen" ist die Konfiguration beim Raspi beendet. <br> <br>

Nun kann man eine Testseite drucken. <br>
 <br>
**Konfiguration Windows Client** <br>
Damit über Cups Printserver gedruckt werden kann, muss der Internetdruckclient und der LPR-Anschlussmonitor über die Windows-Features installiert werden. <br> Möglicherweise muss noch die Firewall angepasst werden und dann kann der Drucker wie gewohnt über die Systemsteuerung an Windows angehängt werden. <br> <br> <br>

Fertig!